package com.example.demo.component;

import com.example.demo.entity.*;
import com.example.demo.mapper.MessageMapper;
import com.example.demo.mapper.MessageSessionMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 21:46
 */
@Component
public class WebSocketController  extends TextWebSocketHandler {

    @Autowired
    private OnlineUserManager onlineUserManager;

    @Autowired
    private MessageMapper messageMapper;

    @Autowired
    private MessageSessionMapper messageSessionMapper;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        System.out.println("[websocket] 连接成功");
        User user = (User) session.getAttributes().get("user");
        if(user==null){
            return;
        }

        onlineUserManager.online(user.getUserId(),session);
        System.out.println("获取到的userId "+ user.getUserId());

    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        System.out.println("[websocket] 收到消息" +message.toString());
//        1。先获取到当前用户的信息
        User user =(User) session.getAttributes().get("user");
        if(user==null){
            System.out.println("[websocket] user ==null  未登录用户，无法进行转发"  );
            return;
        }
//        2.针对请求进行解析 把json格式字符串转换成java中的对象
        MessageRequest req = objectMapper.readValue(message.getPayload(),MessageRequest.class);
        if(req.getType().equals("message")){
//            就进行消息转发

            transferMessage(user,req);

        }else{
            System.out.println("websocket req.type 有误"+message.getPayload());
        }

    }




//    通过这个方法来完成消息的转发操作
    private void transferMessage(User fromUser, MessageRequest req) throws IOException {

//        1。先构造一个待转发的响应对象
        MessageResponse resp = new MessageResponse();
        resp.setType("message");
        resp.setFromId(fromUser.getUserId());
        resp.setFromName(fromUser.getUsername());
        resp.setSessionId(req.getSessionId());
        resp.setContent(req.getContent());





//        把这个java对象转换为json格式字符串
        String respJson = objectMapper.writeValueAsString(resp);
        System.out.println("transferMessage "+ respJson);
//        2。根据这个sessionId  来获取到这个MessageSession 里都有哪些用户，通过查询数据库就知道了

        List<Friend> friends =  messageSessionMapper.getFriendsBySessionId(req.getSessionId(), fromUser.getUserId());

        //        这里有一个群聊的方式 也要把自己添加进去  下面是一个对自己添加的操作
        Friend myself = new Friend();
        myself.setFriendId(fromUser.getUserId());
        myself.setFriendName(fromUser.getUsername());
        friends.add(myself);

//       3. 循环上面的列表，给列表中的每个用户都发送一份响应消息
        for (Friend friend:friends) {
//            知道了每个用户的userId  进一步的查到刚才准备好的OnlineUserManager
            WebSocketSession webSocketSession = onlineUserManager.getSession(friend.getFriendId());
            if(webSocketSession ==null){
//                如果当前用户为不在线，则不发送
                continue;
            }

            webSocketSession.sendMessage(new TextMessage(respJson));
        }

//      4.  把发送的消息构造到数据库中

        Message message = new Message();
        message.setFromId(fromUser.getUserId());
        message.setContent(req.getContent());
        message.setSessionId(req.getSessionId());
        messageMapper.add(message);






    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        System.out.println("[websocket] 连接异常"+exception.toString());
        User user = (User)session.getAttributes().get("user");
        if(user==null){
            return;
        }

        onlineUserManager.offline(user.getUserId(),session);

    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        System.out.println("[websocket] 连接关闭"+status.toString());

        User user = (User)session.getAttributes().get("user");
        if(user==null){
            return;
        }

        onlineUserManager.offline(user.getUserId(),session);
    }
}


