package com.example.demo.controller;

import com.example.demo.entity.Friend;
import com.example.demo.entity.MessageSession;
import com.example.demo.entity.MessageSessionItem;
import com.example.demo.entity.User;
import com.example.demo.mapper.MessageMapper;
import com.example.demo.mapper.MessageSessionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 1:23
 */
@RestController
@ResponseBody
public class MessageSessionController {

    @Autowired
    private MessageSessionMapper messageSessionMapper;

    @Autowired
    private MessageMapper messageMapper;

    @RequestMapping("/sessionlist")
    public Object getSessionList(HttpServletRequest request){

        List<MessageSession> messageSessionList =  new ArrayList<MessageSession>();
//        先获取到userId 从userId 中获取到 所有的sessionId
        HttpSession session = request.getSession(false);
        if(session ==null){
            System.out.println("getsessionlist failed session ==null");
            return messageSessionList;

        }

        User user = (User) session.getAttribute("user");
        if(user==null){
            System.out.println("getsessionlist failed user==null");
            return messageSessionList;

        }

//        获取到所有的sessionId
        List<Integer> sessionlist = messageSessionMapper.getSessionIdByUserId(user.getUserId());
        for (int sessionId: sessionlist) {
            MessageSession messageSession = new MessageSession();
            messageSession.setSessionId(sessionId);

            List<Friend> friends = messageSessionMapper.getFriendsBySessionId(sessionId, user.getUserId());
            messageSession.setFriends(friends);

            String lastMessage = messageMapper.getMessageLastMessage(sessionId);
            if(lastMessage==null){
                messageSession.setLastMessage("");
            }else {
                messageSession.setLastMessage(lastMessage);
            }
            messageSessionList.add(messageSession);
        }
        return messageSessionList;




    }

    @RequestMapping("/session")
    public Object addMessageSession(int toUserId,HttpServletRequest request){
        HashMap<String,Integer> resp = new HashMap<>();

    HttpSession session = request.getSession(false);
        if(session==null){
            System.out.println("session  session==null");
            return new MessageSession();
        }
//往message_session 中插入一条会话  获取该会话的sessionId
        MessageSession messageSession = new MessageSession();
        messageSessionMapper.addMessageSession(messageSession);

        User user = (User) session.getAttribute("user");
        if(user==null){
            return new MessageSession();
        }
        MessageSessionItem messageSessionItem = new MessageSessionItem();
        messageSessionItem.setUserId(user.getUserId());
        messageSessionItem.setSessionId(messageSession.getSessionId());
        messageSessionMapper.addMessageSessionUser(messageSessionItem);

        MessageSessionItem messageSessionItem1 = new MessageSessionItem();
        messageSessionItem1.setUserId(toUserId);
        messageSessionItem1.setSessionId(messageSession.getSessionId());
        messageSessionMapper.addMessageSessionUser(messageSessionItem1);

        System.out.println("[addMessageSession] 新增会话成功! sessionId=" + messageSession.getSessionId()
                + " userId1=" + user.getUserId() + " userId2=" + toUserId);

        resp.put("sessionId", messageSession.getSessionId());
        // 返回的对象是一个普通对象也可以, 或者是一个 Map 也可以, jackson 都能进行处理.
        return resp;


    }



}
