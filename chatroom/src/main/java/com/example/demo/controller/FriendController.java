package com.example.demo.controller;

import com.example.demo.entity.Friend;
import com.example.demo.entity.User;
import com.example.demo.mapper.FriendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 0:35
 */
@RestController
@ResponseBody
public class FriendController {

    @Autowired
    private FriendMapper friendMapper;

//    根据userId来获取到好友列表
    @RequestMapping("/friendlist")
    public  Object getFriendList(HttpServletRequest request){

        HttpSession session = request.getSession(false);
        if(session==null){
            System.out.println("getFriendList failed");
            return new ArrayList<Friend>();
        }

        User user =(User) session.getAttribute("user");
        if(user==null){
            System.out.println("getFriendList fail");
            return new ArrayList<Friend>();
        }

        List<Friend> friendList = friendMapper.getFriendListByUserId(user.getUserId());
        return friendList;


    }


}
