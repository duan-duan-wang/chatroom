package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-20
 * Time: 23:50
 */
@RestController
@ResponseBody
public class UserController {

    @Autowired
    private UserMapper userMapper;

//    登录
    @RequestMapping("/login")
    public Object login(String username, String password, HttpServletRequest request){
//        先判断这个用户是否存在
        User user = userMapper.selectUserByUserName(username);
        if(user==null || !user.getPassword().equals(password)){
            System.out.println("用户名或密码 错误");
            return new User();
        }

//        用户存在
        HttpSession session = request.getSession(true);
        if(session==null){
            System.out.println("session 创建失败");
            return new User();
        }
        session.setAttribute("user",user);
        user.setPassword("");
        return user;

    }

    @RequestMapping("/register")
    public Object register(String username,String password){
        User user =null;
        try {
            user = new User();
            user.setUsername(username);
            user.setPassword(password);

            userMapper.insertUser(user);
            user.setPassword("");
            return user;

        } catch (DuplicateKeyException e) {
            e.printStackTrace();
        }
        return new User();

    }

    @RequestMapping("/userinfo")
    public Object getuserinfo(HttpServletRequest request){

//        先获取session
        HttpSession session = request.getSession(false);
        if(session==null){
            System.out.println("getuserinfo 失败  session==null");
            return new User();
        }

        User user =(User) session.getAttribute("user");
        if(user==null){
            System.out.println("getuserinfo 失败  user==null");
            return  new User();
        }

//        返回user
        user.setPassword("");
        return user;
    }




}
