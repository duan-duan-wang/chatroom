package com.example.demo.controller;

import com.example.demo.entity.Message;
import com.example.demo.mapper.MessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 6:11
 */
@RestController
@ResponseBody
public class MessageController {

    @Autowired
    private MessageMapper messageMapper;

    @RequestMapping("/message")
    public Object getMessage(int sessionId){

        List<Message> messages = messageMapper.getMessagesBySessionId(sessionId);

        Collections.reverse(messages);
        return messages;

    }


}
