package com.example.demo.entity;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 1:10
 */
@Data
public class MessageSession {
    private int  sessionId;
    private List<Friend> friends;
    private String lastMessage;

}
