package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 23:39
 */
@Data
public class MessageRequest {


    private String type="message";
    private int sessionId;
    private String content;

}
