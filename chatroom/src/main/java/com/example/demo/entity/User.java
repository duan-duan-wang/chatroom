package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:使用这个类来表示用户
 * User: Wangduan
 * Date: 2023-07-20
 * Time: 19:27
 */

@Data
public class User {
    private int userId;
    private String username;
    private String password;

}

