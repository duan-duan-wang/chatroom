package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description: 使用这个类来表示用户和会话表中的一条记录
 * message_session_user
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 1:11
 */
@Data
public class MessageSessionItem {

    private int sessionId;
    private int userId;

}
