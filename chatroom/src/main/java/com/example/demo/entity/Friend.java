package com.example.demo.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 0:25
 */
@Data
public class Friend {

    private int friendId;
    private String friendName;

}
