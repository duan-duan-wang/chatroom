package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 6:01
 */
@Data
public class Message {

    private int messageId;
    private int fromId;
    private String fromName;

    private int sessionId;
    private String content;

}



