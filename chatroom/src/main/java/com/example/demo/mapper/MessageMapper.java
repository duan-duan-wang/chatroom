package com.example.demo.mapper;

import com.example.demo.entity.Message;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 5:47
 */
@Mapper
public interface MessageMapper {

    //获取 指定会话的最后一条消息
    String getMessageLastMessage(int sessionId);


//    获取指定会话的历史消息列表
//    只取默认的100条消息
    List<Message> getMessagesBySessionId(int sessionId);

//    WebSocketController中的把转发的消息添加到数据库 用到的方法
    void add(Message message);

}
