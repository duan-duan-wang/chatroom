package com.example.demo.mapper;

import com.example.demo.entity.Friend;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 0:26
 */
@Mapper
public interface FriendMapper {

//    根据当前登录的信息，找到好友列表
    List<Friend> getFriendListByUserId(int userId);
}
