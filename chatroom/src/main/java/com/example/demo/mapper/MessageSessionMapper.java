package com.example.demo.mapper;

import com.example.demo.entity.Friend;
import com.example.demo.entity.MessageSession;
import com.example.demo.entity.MessageSessionItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-21
 * Time: 1:13
 */
@Mapper
public interface MessageSessionMapper {

//    根据userId来查询到所有的sessionId
    List<Integer> getSessionIdByUserId(int userId);

//    根据sessionId查询到所有的会话列表
    List<Friend> getFriendsBySessionId(int sessionId,int selfUserId);

// 插入 message_session表中
    int addMessageSession(MessageSession messageSession);

//    往message_session_user 表中插入
    void addMessageSessionUser(MessageSessionItem messageSessionItem);


}
