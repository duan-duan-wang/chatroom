package com.example.demo.mapper;

import com.example.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Wangduan
 * Date: 2023-07-20
 * Time: 19:35
 */
@Mapper
public interface UserMapper {

//        注册
    int insertUser(User user);

//    根据用户名查询登录信息
    User selectUserByUserName(String username);


}
