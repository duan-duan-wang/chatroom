////////////////////////////////////////////
// 这里实现标签页的切换
////////////////////////////////////////////

function initSwitchTab() {
    // 1. 先获取到相关的元素(标签页的按钮, 会话列表, 好友列表)
    let tabSession = document.querySelector('.tab .tab-session');
    let tabFriend = document.querySelector('.tab .tab-friend');
    // querySelectorAll 可以同时选中多个元素. 得到的结果是个数组
    // [0] 就是会话列表
    // [1] 就是好友列表
    let lists = document.querySelectorAll('.list');
    // 2. 针对标签页按钮, 注册点击事件. 
    //    如果是点击 会话标签按钮, 就把会话标签按钮的背景图片进行设置. 
    //    同时把会话列表显示出来, 把好友列表隐藏
    //    如果是点击 好友标签按钮, 就把好友标签按钮的背景图片进行设置. 
    //    同时把好友列表显示出来, 把会话列表进行隐藏
    tabSession.onclick = function() {
        // a) 设置图标
        tabSession.style.backgroundImage = 'url(img/对话.png)';
        tabFriend.style.backgroundImage = 'url(img/用户2.png)';
        // b) 让会话列表显示出来, 让好友列表进行隐藏
        lists[0].classList = 'list';
        lists[1].classList = 'list hide';
    }

    tabFriend.onclick = function() {
        // a) 设置图标
        tabSession.style.backgroundImage = 'url(img/对话2.png)';
        tabFriend.style.backgroundImage = 'url(img/用户.png)'
        // b) 让好友列表显示, 让会话列表隐藏
        lists[0].classList = 'list hide';
        lists[1].classList = 'list';
    }
}

initSwitchTab();

// 获取到当前登录用户

function getuserinfo(){

    $.ajax({

        type:'get',
        url:'userinfo',
        success:function(body){

            if(body&&body.userId>0){
            let userDiv = document.querySelector('.main .left .user');
            userDiv.innerHTML = body.username;
            userDiv.setAttribute("user-id",body.userId);

            }else{
                alert("当前用户未登录");
                location.assign('/login.html');
            }
            
        }



    });
}
getuserinfo();


// 获取到当前登录用户的好友列表
function getFriendList(){
   

    $.ajax({
        type:'get',
        url:'friendlist',
        success:function(body){
            let friendlistUL= document.querySelector('#friend-list');
            friendlistUL.innerHTML="";
            for( let friend of body){
               

                let li = document.createElement('li');
                li.innerHTML = '<h4>'+friend.friendName+'</h4>';
    
                li.setAttribute("friend-id",friend.friendId);
                friendlistUL.appendChild(li);


                li.onclick =function(){
                    clickFriend(friend);
                }

            }
            
        }



    });

}
getFriendList();

// 获取到会话列表
// 并设置点击高亮
function getSessionList(){

   

    $.ajax({
        type:'get',
        url:'sessionlist',
        success:function(body){


            let sessionListUL = document.querySelector("#session-list");
            sessionListUL.innerHTML = "";
        for(let session of body){

   // 针对 lastMessage 的长度进行截断处理
   if (session.lastMessage.length > 10) {
    session.lastMessage = session.lastMessage.substring(0, 10) + '...';
}
           
            let li = document.createElement('li');
            li.setAttribute("message-session-id",session.sessionId);
            li.innerHTML ='<h3>'+session.friends[0].friendName+'<h3>'+
            '<p>'+session.lastMessage+'</p>';

            
            sessionListUL.appendChild(li);
            
            li.onclick = function(){

                clickSession(li);

            }

        }
        }

    });

}
getSessionList();

// 设置高亮

function clickSession(currentLi){

    // 设置高亮
    let allLis = document.querySelectorAll('#session-list>li');
    activeSession(allLis, currentLi);

    // 获取历史消息
    let sessionId = currentLi.getAttribute("message-session-id");
    getHistoryMessage(sessionId);



}

function getHistoryMessage(sessionId){

    console.log("获取历史消息 sessionId = "+sessionId);
    let titleDiv  = document.querySelector('.right .title');
    titleDiv.innerHTML="";
    let messageShowDiv = document.querySelector('.right .message-show');
    messageShowDiv.innerHTML="";

    // 2、重新设置会话标题
    // 先明确当前选中的是谁
    let selectedH3 = document.querySelector('#session-list .selected>h3');
    if(selectedH3){
        titleDiv.innerHTML = selectedH3.innerHTML;
    }


    $.ajax({
        type:'get',
        url:'message?sessionId='+sessionId,
        success:function(body){
            // 先进行清空操作
          
            
            for(let message of body){

                addMessage(messageShowDiv,message);

            }
            scrollBottom(messageShowDiv);

        }
    });
}

// 把 messageShowDiv 里的内容滚动到底部. 
function scrollBottom(elem) {
    // 1. 获取到可视区域的高度
    let clientHeight = elem.offsetHeight;
    // 2. 获取到内容的总高度
    let scrollHeight = elem.scrollHeight;
    // 3. 进行滚动操作, 第一个参数是水平方向滚动的尺寸. 第二个参数是垂直方向滚动的尺寸
    elem.scrollTo(0, scrollHeight - clientHeight);
}

function addMessage(messageShowDiv,message){
    let messageDiv = document.createElement('div');
    // 根据信息是不是自己发的来决定靠左还是靠右
    let selfUsername = document.querySelector('.left .user').innerHTML;
    if(selfUsername == message.fromName){
        // 消息是自己发的 靠右

        messageDiv.className = 'message message-right';

    }else{


        // 消息不是自己发的 靠左
        messageDiv.className = 'message message-left';


    }

    messageDiv.innerHTML ='<div class="box">'
    +'<h4>'+message.fromName+'</h4>'
    +'<p>'+message.content+'</p>'
    +'</div>';

    messageShowDiv.appendChild(messageDiv);
    



}

function activeSession(allLis, currentLi) {
    // 这里的循环遍历, 更主要的目的是取消未被选中的 li 标签的 className
    for (let li of allLis) {
        if (li == currentLi) {
            li.className = 'selected';
        } else {
            li.className = '';
        }
    }
}

function clickFriend(friend){


    console.log('点击好友:', friend);
    // 先要在所有的列表中查询friend对应的会话是否存在
    let sessionLi = findSessionByName(friend.friendName);
    let sessionListUL = document.querySelector('#session-list');

    if(sessionLi){
  // 存在就给这个好友模拟一个点击事件

        sessionListUL.insertBefore(sessionLi,sessionListUL.children[0]);

        sessionLi.click();

    }else{

    // 不存在就创建新的会话 并添加模拟点击事件，再模拟
        sessionLi =document.createElement('li');
        sessionLi.innerHTML = '<h3>'+friend.friendName+'</h3>'+'<p></p>';
        sessionListUL.insertBefore(sessionLi,sessionListUL.children[0]);

        sessionLi.onclick= function(){
            clickSession(sessionLi);
        }
        sessionLi.click();


        // 告知服务器创建新的会话
        createSession(friend.friendId,sessionLi);


    }

    // 建立一个标签页的跳转
    let tabSession = document.querySelector('.tab .tab-session');
    tabSession.click();
}


function  findSessionByName(username){
    let sessionListUL = document.querySelectorAll('#session-list>li');

    for(let sessionLi of sessionListUL){
        let h3 = sessionLi.querySelector('h3');
        if(h3.innerHTML==username){
            return sessionLi;
        }
    }

    return null;


}

function createSession(friendId,sessionLi){

$.ajax({

    type:'post',
    url:'session?toUserId='+friendId,
    success:function(body){
        console.log("创建会话成功! sessionId = "+body.sessionId);
        
        sessionLi.setAttribute("message-session-id",body.sessionId);
       


    },error:function(body){
        console.log('会话创建失败!');
    }



});

}



// ///////////////////////////////////////
// 操作websocket
// ///////////////////////////////////////
let websocket = new WebSocket("ws://127.0.0.1:8080/WebSocketMessage");

websocket.onopen = function(){
    console.log("websocket 连接成功！");

}

websocket.onclose = function(){
    console.log("websocket 连接断开！");
    
}

websocket.onerror = function(){
    console.log("websocket 连接异常！");
    
}

websocket.onmessage = function(e){
    console.log("websocket 收到消息！"+e.data);

    // 这里收到的消息是一个字符串，要转换为js对象
    let resp  = JSON.parse(e.data);
    if(resp.type == 'message'){
        handleMessage(resp);
    }else{
        console.log("resp.type 不符合要求");
    }

    
}

function handleMessage(resp) {
    // 把客户端收到的消息, 给展示出来. 
    // 展示到对应的会话预览区域, 以及右侧消息列表中. 

    // 1. 根据响应中的 sessionId 获取到当前会话对应的 li 标签. 
    //    如果 li 标签不存在, 则创建一个新的
    let curSessionLi = findSessionLi(resp.sessionId);
    if (curSessionLi == null) {
        // 就需要创建出一个新的 li 标签, 表示新会话. 
        curSessionLi = document.createElement('li');
        curSessionLi.setAttribute('message-session-id', resp.sessionId);
        // 此处 p 标签内部应该放消息的预览内容. 一会后面统一完成, 这里先置空
        curSessionLi.innerHTML = '<h3>' + resp.fromName + '</h3>'
            + '<p></p>';
        // 给这个 li 标签也加上点击事件的处理
        curSessionLi.onclick = function() {
            clickSession(curSessionLi);
        }
    }
    // 2. 把新的消息, 显示到会话的预览区域 (li 标签里的 p 标签中)
    //    如果消息太长, 就需要进行截断. 
    let p = curSessionLi.querySelector('p');
    p.innerHTML = resp.content;
    if (p.innerHTML.length > 10) {
        p.innerHTML = p.innerHTML.substring(0, 10) + '...';
    }
    // 3. 把收到消息的会话, 给放到会话列表最上面. 
    let sessionListUL = document.querySelector('#session-list');
    sessionListUL.insertBefore(curSessionLi, sessionListUL.children[0]);
    // 4. 如果当前收到消息的会话处于被选中状态, 则把当前的消息给放到右侧消息列表中. 
    //    新增消息的同时, 注意调整滚动条的位置, 保证新消息虽然在底部, 但是能够被用户直接看到. 
    if (curSessionLi.className == 'selected') {
        // 把消息列表添加一个新消息. 
        let messageShowDiv = document.querySelector('.right .message-show');
        addMessage(messageShowDiv, resp);
        scrollBottom(messageShowDiv);
    }
    // 其他操作, 还可以在会话窗口上给个提示 (红色的数字, 有几条消息未读), 还可以播放个提示音.  
    // 这些操作都是纯前端的. 实现也不难, 不是咱们的重点工作. 暂时不做了. 
}

function findSessionLi(targetSessionId) {
    // 获取到所有的会话列表中的 li 标签
    let sessionLis = document.querySelectorAll('#session-list li');
    for (let li of sessionLis) {
        let sessionId = li.getAttribute('message-session-id');
        if (sessionId == targetSessionId) {
            return li;
        }
    }
    // 啥时候会触发这个操作, 就比如如果当前新的用户直接给当前用户发送消息, 此时没存在现成的 li 标签
    return null;
}


// ///////////////////////////////////////
//实现消息发送/接收逻辑
// ///////////////////////////////////////
function initSendButton(){
    // 1.获取到发送按钮  和  消息输入框
    let senButton = document.querySelector('.right .ctrl button');
    let messageInput = document.querySelector('.right .message-input');
    // 2.给发送按钮注册一个点击事件
    senButton.onclick = function(){

        if(!messageInput.value){

            return;
        }

        // 获取到当前选中的li标签的sessionId
        let selectedLi = document.querySelector('#session-list .selected');
        if(selectedLi==null){
            // 当前没有标签被选中
            return;
        }

        let sessionId = selectedLi.getAttribute("message-session-id");

        // 构造一个json数据
         
         let req = {
            type:'message',
            sessionId:sessionId,
            content:messageInput.value
         };
         req = JSON.stringify(req);
         websocket.send(req);
         messageInput.value='';


    }
 
}
initSendButton();





